"use strict";

/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef:true */
/*jslint vars:true */
/*jslint plusplus: true */

var express = require('express');
var path = require('path');
var logger = require('morgan');
var bodyParser = require('body-parser');

var routes = require('./routes/index');


var app = express();

app.use(logger('dev'));
app.use(bodyParser.json());

app.use('/', routes);


module.exports = app;
