/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef:true */
/*jslint vars:true */
/*jslint plusplus: true */
/*jslint es5: true */

(function () {
    "use strict";
    
    // METHODS
    var handleErrors;
    
    var express = require('express');
    var router = express.Router();
    var reqParam = require('../../Data/Enums/requestParamEnum.js');
    var mediator = require('../../Business/mediator').getMediator(true);
    
    // REGISTER ROUTES

    router.post('/publish', function (req, res) {
        console.log('route: /publish hit!');
        
        handleErrors(req, res, function (channel, data) {
            mediator.publish(channel, data);
            
            res.end('You published to channel: ' + channel);
        });
    });

    router.post('/subscribe', function (req, res) {
        console.log('route: /subscribe hit!');
        
        handleErrors(req, res, function (channel, data) {
            mediator.subscribe(channel, data);
            
            res.end('You published to channel: ' + channel);
        });
    });
    
    
    
    // PRIVATE METHODS
    
    handleErrors = function (req, res, callback) {
        try {
            var channel = req.param(reqParam.CHANNEL);
            if (!channel) {
                res.status(401);
                res.send('Request does not contain required parameter "channel"');
                return;
            }
            var data = req.param(reqParam.DATA);
            if (!data) {
                res.status(401);
                res.send('Request does not contain required parameter "data"');
                return;
            }
            
            callback(channel, data);
            
        } catch (err) {
            console.log('ERROR!');
            console.log(err);
            res.status(500);
            res.send(err);
            return;
        }
    };

    module.exports = router;
}());