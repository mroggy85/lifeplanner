/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef:true */
/*jslint vars:true */
/*jslint plusplus: true */
/*jslint es5: true */

(function () {
    "use strict";
    
    // IMPORTS
    var express = require('express');
    var router = express.Router();
    var requestParamEnum = require('../../Data/Enums/requestParamEnum.js');
    var headerTypeEnum = require('../../Data/Enums/headerTypeEnum.js');
    var headerValueEnum = require('../../Data/Enums/headerValueEnum.js');
    var reminderManager = require('../../Business/reminderManager.js').getReminderManager();
    
    // PRIVATE MEMBERS
    var _reqParamEnum = requestParamEnum;
    
    // METHODS
    var handleErrorsWithID, handleErrorsWithData;
    
    // CONSTANTS
    var ERROR_MSG_REQ_NO_ID = 'Request does not contain required parameter ' + _reqParamEnum.ID;
    var ERROR_MSG_REQ_NO_DATA = 'Request does not contain required parameter ' + _reqParamEnum.DATA;
    
    // REGISTER ROUTES

    // --> GET
    router.get('/', function (req, res) {
        console.log('route: GET-ONE');
        
        handleErrorsWithID(req, res, function (id) {
            reminderManager.getReminderByID(id, function (item) {
                res.setHeader(headerTypeEnum.CONTENT_TYPE, headerValueEnum.JSON);
                res.end(JSON.stringify(item));
            });
        });
    });
    
    // --> GET
    router.get('/all', function (req, res) {
        console.log('route: GET-ALL');
        
        reminderManager.getAllReminders(function (items) {
            res.setHeader(headerTypeEnum.CONTENT_TYPE, headerValueEnum.JSON);
            res.end(JSON.stringify(items));
        });
    });

    // --> POST
    router.post('/', function (req, res) {
        console.log('route: POST');
        
        handleErrorsWithData(req, res, function (data) {
            reminderManager.createReminder(data, function (item) {
                res.setHeader(headerTypeEnum.CONTENT_TYPE, headerValueEnum.JSON);
                res.end(JSON.stringify(item));
            });
        });
    });
    
    // --> PUT (update)
    router.put('/', function (req, res) {
        console.log('route: PUT');
        
        handleErrorsWithData(req, res, function (data) {
            reminderManager.changeReminder(data, function (item) {
                res.setHeader(headerTypeEnum.CONTENT_TYPE, headerValueEnum.JSON);
                res.end(JSON.stringify(item));
            });
        });
    });
             
    // --> DELETE
    router.delete('/', function (req, res) {
        console.log('route: DELETE');
        
        handleErrorsWithID(req, res, function (id) {
            reminderManager.removeReminderByID(id, function (resultBool) {
                res.setHeader(headerTypeEnum.CONTENT_TYPE, headerValueEnum.JSON);
                res.end({ result: resultBool});
            });
        });
    });
    
    // PRIVATE METHODS
    
    handleErrorsWithID = function (req, res, callback) {
        try {
            var id = req.param(_reqParamEnum.ID);
            if (!id) {
                res.status(401);
                var errorMsg = ERROR_MSG_REQ_NO_ID;
                console.log(errorMsg);
                res.send(errorMsg);
                return;
            }
            
            callback(id);
            
        } catch (err) {
            console.log('ERROR!');
            console.error(err);
            res.status(500);
            res.send(err);
            return;
        }
    };
    
    handleErrorsWithData = function (req, res, callback) {
        try {
            var data = req.param(_reqParamEnum.DATA);
            if (!data) {
                res.status(401);
                var errorMsg = ERROR_MSG_REQ_NO_DATA;
                console.log(errorMsg);
                res.send(errorMsg);
                return;
            }
            
            console.dir(data);
            callback(data);
            
        } catch (err) {
            console.log('ERROR!');
            console.error(err);
            res.status(500);
            res.send(err);
            return;
        }
    };

    module.exports = router;
}());