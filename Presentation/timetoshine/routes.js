/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef:true */
/*jslint vars:true */
/*jslint plusplus: true */
/*jslint es5: true */

(function () {
    "use strict";
    
    // METHODS
    var handleErrors;
    
    var express = require('express');
    var router = express.Router();
    var reqParam = require('../../Data/Enums/requestParamEnum.js');
    
    // REGISTER ROUTES

    router.post('/', function (req, res) {
        console.log('route: POST');
        
        handleErrors(req, res, function (data) {
            
        });
    });
    
    
    
    // PRIVATE METHODS
    
    handleErrors = function (req, res, callback) {
        try {
            var data = req.param(reqParam.DATA);
            if (!data) {
                res.status(401);
                res.send('Request does not contain required parameter "data"');
                return;
            }
            
            callback(data);
            
        } catch (err) {
            console.log('ERROR!');
            console.log(err);
            res.status(500);
            res.send(err);
            return;
        }
    };

    module.exports = router;
}());