/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef:true */
/*jslint vars:true */
/*jslint plusplus: true */

/* Page Renderer
 * --------------------
 * Wrapper around the method "res.render()
 * It also is a convenient way to render 
 * with layout to avoid duplication of code
 */
var pageRenderer = function () {
    "use strict";
    
    // IMPORTS
    
    // PRIVATE MEMBERS
    
    // METHODS
    //-- public
    var RenderPage;
    //-- private
    var getLayout;
    
    // INIT
    var init = function () {
        
    };
    
    // PUBLIC METHODS
    
    RenderPage = function (htmlDocName, viewModel, res) {
        res.render(htmlDocName, viewModel, function (err, html) {
            if (err) {
                throw new Error(err);
            }
            getLayout(res, html);
        });
    };
    
    
    // PRIVATE METHODS
    
    getLayout = function (res, content) {
        res.render('_layout', { body: content});
    };
    
    // FASCADE
    var fascade = {
        
        "renderPage": RenderPage
    };
    
    init();
    
    return fascade;
};

module.exports.getPageRenderer = pageRenderer;