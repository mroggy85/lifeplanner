/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef:true */
/*jslint vars:true */
/*jslint plusplus: true */
/*jslint es5: true */
/*jslint white: true */

var getRouter = function (apiRequester) {
    "use strict";
    
     // IMPORTS
    var express = require('express');
    var router = express.Router();
    var pageRenderer = require('../helpers/pageRenderer.js');
    
    // PRIVATE MEMBERS
    var _api = apiRequester,
        _pageRenderer = pageRenderer.getPageRenderer();
    
    // METHODS
    var renderAllReminders, 
        changeReminder, 
        createReminder;
    
    // POST
    router.post('/', function (req, res) {
        if (req.body.change) {
            changeReminder(req.body, res);
        }
        else if (req.body.create) {
            createReminder(req.body, res);
        }
        else {
            res.end('something went wrong');
        }
    });
    
    renderAllReminders = function (res) {
        _api.getAllReminders(function (items) {
            _pageRenderer.renderPage('index', { items: items }, res);
        });
    };
    
    changeReminder = function (dataFromUser, res) {
        _api.changeReminder(dataFromUser, function (result) {
            renderAllReminders(res);
        });
    };
    
    createReminder = function (dataFromUser, res) {
        _api.createReminder(dataFromUser, function (result) {
            renderAllReminders(res);
        });
    };
    
    return router;
};

module.exports.getRouter = getRouter;
