/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef:true */
/*jslint vars:true */
/*jslint plusplus: true */
/*jslint es5: true */
/*jslint white: true */

var getRouter = function (apiRequester) {
    "use strict";
    
    // IMPORTS
    var express = require('express');
    var router = express.Router();
    var pageRenderer = require('../helpers/pageRenderer.js');
    
    // PRIVATE MEMBERS
    var _pageRenderer = pageRenderer.getPageRenderer(),
        _api = apiRequester;
    
    // METHODS
    var renderChangeReminder, deleteReminder, renderAllReminders;
    
    // GET
    
    // -- index
    router.get('/', function (req, res) {
        renderAllReminders(res);
    });
    
    // -- create (new reminder)
    router.get('/create', function (req, res) {
        _pageRenderer.renderPage('item', { }, res);
    });
    
    // -- about
    router.get('/about', function (req, res) {
        _pageRenderer.renderPage('about', { }, res);
    });
    
    
    // POST
    
    // -- change or delete
    router.post('/', function (req, res) {
        var id = req.body.id;
        if (req.body.change) {
            renderChangeReminder(id, res);
        }
        else if (req.body.delete) {
            deleteReminder(id, res);
        }
        else {
            res.end('something went wrong');
        }
    });
    
    // PRIVATE METHODS
    
    renderChangeReminder = function (id, res) {
        _api.getReminderByID(id, function (item) {
            _pageRenderer.renderPage('item', { item: item }, res);
        });
    };
    
    deleteReminder = function (id, res) {
        _api.deleteReminder(id, function (response) {
            renderAllReminders(res);
        });
    };
    
    renderAllReminders = function (res) {
        _api.getAllReminders(function (items) {
            _pageRenderer.renderPage('index', { items: items }, res);
        });
    };
    
    return router;
};

module.exports.getRouter = getRouter;
