/*jslint browser: true*/
/*global $, jQuery, alert*/
/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef:true */
/*jslint vars:true */
/*jslint plusplus: true */
/*jslint es5: true */

(function () {
    "use strict";
    
    var removeTask, createTask, changeTask;
    
    $('input').on('click', function (event) {
        var form, id;
        var type = event.target.value.toLowerCase();
        switch (type) {
        case "create":
            form = event.target.parentNode;
            createTask(form);
            break;
        case "change":
            id = event.target.parentNode.name;
            changeTask(id);
            break;
        case "delete":
            id = event.target.parentNode.name;
            removeTask(id);
            break;
        }
        return false;
    });
    
    removeTask = function (id) {
        $.ajax({
            type: "DELETE",
            url: "/Task",
            data: { id: id}
        });
    };
    
    changeTask = function (id) {
        $.ajax({
            type: "PUT",
            url: "/Task",
            data: { id: id}
        }).done(function (data) {
            $('#content').html(data);
        });
    };
    
    createTask = function (form) {
        $.ajax({
            type: "POST",
            url: "/Task",
            data: {
                title: form.title.value,
                description: form.description.value,
                dateToShow: form.datetoshow.value,
                timeToShow: form.timetoshow.value,
                priority: form.priority.value,
                category: form.category.value,
                interval: form.interval
            }
        });
    };
}());