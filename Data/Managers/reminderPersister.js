/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef: true */
/*jslint vars: true */
/*jslint plusplus: true */
/*jslint es5: true */

var events = require('events'),
    MongoClient = require('mongodb'),
    dataPersister = require('./dataPersister.js');

// Wrapper around MongoDB driver
// CRUD operations against the DB
// --------------------------------
var reminderPersister = function (dbName, collectionName) {
    "use strict";
    
    // PRIVATE MEMBERS
    var _base;
    
    // CONSTANTS
    var DEFAULT_DB = "lifeplanner";
    var DEFAULT_COLLECTION = "tasks";
    
    // METHODS
    var GetRemindersAfterDate, GetRemindersBeforeDate, GetAllReminders, CreateNewReminder, CreateNewNotification;
    var saveReminder, insertReminder, updateReminder, find, findOne, findByDateBefore, findByDateAfter,
        removeOne, docsToArray, connectToDb;
    
    var init = function () {
        console.log('RMDR_P: Init');
        
        var dbNameToUse = dbName || DEFAULT_DB;
        var dbCollectionToUse = collectionName || DEFAULT_COLLECTION;
        _base = dataPersister.getDataPersister(dbNameToUse, dbCollectionToUse);
    };
    
    // PUBLIC METHODS
    
    GetRemindersAfterDate = function (date, callback) {
        console.log('RMDR_P: reading from DB...');
        
        findByDateAfter(date, callback);
    };
    
    GetRemindersBeforeDate = function (date, callback) {
        console.log('RMDR_P: reading from DB...');
        
        findByDateBefore(date, callback);
    };
    
    CreateNewReminder = function (obj, callback) {
        _base.create(obj, callback);
    };
    
    CreateNewNotification = function (obj, callback) {
        _base.create(obj, callback, 'toBeNotified');
    };
    
    // PRIVATE METHODS
        
    findByDateBefore = function (date, callback) {
        _base.getAllByCriteria({dateToShow: {$lt: date}}, callback);
    };
    
    findByDateAfter = function (date, callback) {
        _base.getAllByCriteria({dateToShow: {$gt: date}}, callback);
    };
    
    init();
    
    // FASCADE
    var fascade = {
        "create": _base.create,
        "read": _base.read,
        "update": _base.update,
        "remove": _base.remove,
        
        "createNewReminder": CreateNewReminder,
        "createNewNotification": CreateNewNotification,
        
        "GetRemindersAfterDate": GetRemindersAfterDate,
        "GetRemindersBeforeDate": GetRemindersBeforeDate,
        
        "getAll": _base.getAll
    };
    
    return fascade;
};

module.exports.getReminderPersister = reminderPersister;