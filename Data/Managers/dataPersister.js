/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef: true */
/*jslint vars: true */
/*jslint plusplus: true */
/*jslint es5: true */
/*jslint white: true */

var events = require('events'),
    MongoClient = require('mongodb');

// Wrapper around MongoDB driver
// CRUD operations against the DB
// --------------------------------
var dataPersister = function (dbName, collectionName) {
    "use strict";
    
    // PRIVATE MEMBERS
    var _mongoClient = MongoClient.MongoClient;
    var _db, _collection;
    var _connecting = false;
    
    // CONSTANTS
    var DEFAULT_DB = "lifeplanner";
    var DB_HOST = "mongodb://localhost:27017/";
    var DB_PATH = (dbName !== undefined ? dbName : DEFAULT_DB);
    var DB_URI = DB_HOST + DB_PATH;
    var DEFAULT_COLLECTION = "tasks";
    var DB_COLLECTION = (collectionName !== undefined ? collectionName : DEFAULT_COLLECTION);
    
    // METHODS
    var Create, Read, Update, Remove,
        GetRemindersAfterDate, GetRemindersBeforeDate, GetAll, GetAllByCriteria;
    var save, insert, update, findAll, findByCriteria, findOne, findByDateBefore, findByDateAfter,
        removeOne, docsToArray, connectToDb, getCollection;
    
    var init = function () {
        console.log('DATA_P: Init');
        
        connectToDb();
    };
    
    // PUBLIC METHODS
    
    Create = function (obj, callback, collection) {
        console.log('DATA_P: adding to DB...');
        
        save(obj, callback, collection);
    };
    
    Read = function (id, callback) {
        console.log('DATA_P: reading from DB...');
        
        findOne(id, callback);
    };
    
    Update = function (obj, callback) {
        console.log('DATA_P: updating from DB...');
        
        update(obj, callback);
    };
    
    Remove = function (id, callback) {
        console.log('DATA_P: removing from DB...');
        
        removeOne(id, callback);
    };
    
    GetRemindersAfterDate = function (date, callback) {
        console.log('DATA_P: reading from DB...');
        
        findByDateAfter(date, callback);
    };
    
    GetRemindersBeforeDate = function (date, callback) {
        console.log('DATA_P: reading from DB...');
        
        findByDateBefore(date, callback);
    };
    
    GetAll = function (callback) {
        console.log('DATA_P: getting all from DB...');
        
        findAll(callback);
    };
    
    GetAllByCriteria = function (criteria, callback) {
        console.log('DATA_P: getting all by criteria from DB...');
        
        findByCriteria(criteria, callback);
    };
    
    
    // PRIVATE METHODS
    
    save = function (obj, callback, collection) {
        var coll = getCollection(collection);
        coll.save(obj, {w: 1}, function (err, result) {
            if (err) {
                throw new Error(err);
            }

            callback(obj);
        });
    };
    
    insert = function (obj, callback, collection) {
        var coll = getCollection(collection);
        coll.insert(obj, {w: 1}, function (err, result) {
            if (err) {
                throw new Error(err);
            }

            callback(obj);
        });
    };
    
    update = function (obj, callback, collection) {
        var coll = getCollection(collection);
        coll.update({ _id: obj._id }, obj, function (err, result) {
            if (err) {
                throw new Error(err);
            }
            console.log(result);
            
            callback(obj);
        });
    };
    
    findAll = function (callback, collection) {
        var coll = getCollection(collection);
        coll.find({}, function (err, docs) {
            if (err) {
                throw new Error(err);
            }
            docsToArray(docs, callback);
        });
    };
    
    findByCriteria = function (criteria, callback, collection) {
        var coll = getCollection(collection);
        coll.find(criteria, function (err, docs) {
            if (err) {
                throw new Error(err);
            }
            docsToArray(docs, callback);
        });
    };
    
    findOne = function (id, callback, collection) {
        var coll = getCollection(collection);
        coll.findOne({ _id: id}, function (err, item) {
            if (err) {
                throw new Error(err);
            }
            callback(item);
        });
    };
    
    
    
    removeOne = function (id, callback, collection) {
        if (!id) {
            throw new Error("id cannot be null");
        }
        
        var coll = getCollection(collection);
        coll.remove({ _id: id}, {w: 1}, function (err, numberOfRemovedDocs) {
            if (err) {
                throw new Error(err);
            }
            var result = (numberOfRemovedDocs === 1);
            callback(result);
        });
    };
    
    docsToArray = function (docs, callback) {
        docs.toArray(function (err, docs) {
            if (err) {
                throw new Error(err);
            }
            callback(docs);
        });
    };
    
    connectToDb = function () {
        console.log('DATA_P: connecting to DB...');
        MongoClient.connect(DB_URI, function (err, db) {
            if (err) {
                console.log('DATA_P: error connection to DB:');
                console.dir(err);
                return;
            }
            console.log('DATA_P: connected to DB!');
        
            _db = db;
            _collection = db.collection(DB_COLLECTION);
        });
    };
    
    getCollection = function (collection) {
        if (collection) {
            return _db.collection(collection);
        }
        else {
            return _collection;
        }
        
    };
    
    // FASCADE
    var fascade = {
        "create": Create,
        "read": Read,
        "update": Update,
        "remove": Remove,
        
        "getAll": GetAll,
        "getAllByCriteria": GetAllByCriteria
    };
    
    init();
    
    return fascade;
};

module.exports.getDataPersister = dataPersister;