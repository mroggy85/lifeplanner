/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef:true */
/*jslint vars:true */
/*jslint plusplus: true */
/*jslint es5: true */

var eventsEnum = {
    DONE: '0',
    FOUND: '1',
    FAILED: '2',
    ERROR: '3'
};

module.exports.eventsEnum = eventsEnum;