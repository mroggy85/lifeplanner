/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef:true */
/*jslint vars:true */
/*jslint plusplus: true */
/*jslint es5: true */

var headerTypeEnum = {
    CONTENT_TYPE: 'Content-Type',
    ACCEPTS: 'Accepts'
};

module.exports = headerTypeEnum;