/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef:true */
/*jslint vars:true */
/*jslint plusplus: true */
/*jslint es5: true */

var requestParamEnum = {
    "CHANNEL": "channel",
    "DATA": "data",
    "ID": "id"
};

module.exports = requestParamEnum;