/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef:true */
/*jslint vars:true */
/*jslint plusplus: true */
/*jslint es5: true */

var intervalTypeEnum = {
    "DAYS": 0,
    "WEEKDAY": 1,
    "DATE_IN_MONTH": 2,
    "SPECIAL": 3
};

module.exports.intervalTypeEnum = intervalTypeEnum;