/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef:true */
/*jslint vars:true */
/*jslint plusplus: true */
/*jslint es5: true */

var headerValueEnum = {
    JSON: 'application/json',
};

module.exports = headerValueEnum;