/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef:true */
/*jslint vars:true */
/*jslint plusplus: true */
/*jslint es5: true */

var statusEnum = {
    "PENDING": 0,
    "COMPLETED": 1,
    "ABORTED": 2
};

module.exports.statusEnum = statusEnum;