/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef:true */
/*jslint vars:true */
/*jslint plusplus: true */
/*jslint es5: true */

var interval = {
    "type": -1, // Enum: intervalTypeEnum
    "data": null, // info: depending on type
    "timeToShow": null // 00:00 
};

module.exports.interval = interval;