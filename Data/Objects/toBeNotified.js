/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef:true */
/*jslint vars:true */
/*jslint plusplus: true */
/*jslint es5: true */

var toBeNotified = {
    "reminderID": "00000000-0000-0000-0000-000000000000", // Guid
    
    "dateToShow": null, // ISO DATE (MongoDB Date)   
    "timeToShow": null, // 00:00
};

module.exports.DefaultObject = toBeNotified;