/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef:true */
/*jslint vars:true */
/*jslint plusplus: true */

/* Comment of class
 *
 */
var [className] = function () {
    "use strict";
    
    // IMPORTS
    
    // PRIVATE MEMBERS
    
    // METHODS
    
    // INIT
    var init = function () {
        
    };
    
    // PUBLIC METHODS
    
    var [PublicMethod] = function () {

    };
    
    
    // PRIVATE METHODS
    
    var [privateMethod] = function () {

    };
    
    // FASCADE
    var fascade = {
        
        "events": _eventEmitter
    };
    
    init();
    
    return fascade;
};

module.exports.[nameOfProperty] = [className];