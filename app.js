/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef:true */
/*jslint vars:true */
/*jslint plusplus: true */

/* Just for testing3 of Team City... */
(function () {
    "use strict";
    
    console.log('APP: Init');
    var http = require('http'),
        events = require('events'),
        _eventEmitter = new events.EventEmitter();

    var factory = require('./Business/Helpers/factory.js').getFactory();
    var taskManager = require('./Business/taskManager.js').getTaskManager();
    var eventsEnum = require('./Data/Enums/eventsEnum.js').eventsEnum;

    var response = function (req, res) {
        res.writeHead(200, {'Content-Type': 'text/plain'});
        console.log('request received!');

        res.end('Hello World\n');
    };

    http.createServer(response).listen(1337, '127.0.0.1');

    var taskID;

    console.log('APP: Server running at http://127.0.0.1:1337/');

    taskManager.events.on(eventsEnum.ERROR, function (error) {
        console.log('APP: event ERROR Detected...');
        console.log(error);
    });
    taskManager.events.on(eventsEnum.DONE, function (task) {
        console.log('APP: event DONE Detected...');
        console.log('APP: Task saved!');
        console.dir(task);
        
        console.log('APP: get task...');
        taskManager.getTaskByID(task.id);
    });
    taskManager.events.on(eventsEnum.FOUND, function (task) {
        console.log('APP: event FOUND Detected...');
        console.log('APP: Task retrieved!');
        console.dir(task);
    });

    console.log('APP: create task...');
    taskManager.createTask({ title: 'tja' });
}());