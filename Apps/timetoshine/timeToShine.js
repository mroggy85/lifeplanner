/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef:true */
/*jslint vars:true */
/*jslint plusplus: true */

(function () {
    "use strict";
    
    var PORT = process.env.PORT || 6064;
    var EVERY_SECOND = 1000;
    var EVERY_TEN_SECOND = 10000;
    var EVERY_MINUTE = 60000;
    
    console.log('TtS: Init');

    var express = require('express');
    var path = require('path');
    var logger = require('morgan');
    var bodyParser = require('body-parser');
    var routes = require('../../Presentation/timetoshine/routes.js');
    var reminderMonitor = require('../../Business/reminderMonitor.js').getReminderMonitor();
    
    var app = express();

    app.use(logger('dev'));
    app.use(bodyParser.json());

    app.use('/', routes);

    app.set('port', PORT);
    
    app.listen(app.get('port'), function () {
        console.log('TsT server listening on port ' + PORT);
    });
    
    setInterval(function () {
        console.log('TtS: Check');
        reminderMonitor.check();
        
    }, EVERY_MINUTE);
}());