package com.example.oskar.lifeplanner;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.example.oskar.lifeplanner.R;

public class CreateNewNotification extends Activity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_task);

        final Button button1 = (Button) findViewById(R.id.btnSend);
        button1.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                sendNewNotificationToServer();
            }
        });

        final Button button2 = (Button) findViewById(R.id.btnBack);
        button2.setOnClickListener(new View.OnClickListener() {
            public void onClick(View v) {
                switchToNotificationList();
            }
        });
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.create_new_notification, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void switchToNotificationList()
    {
        finish();
//        try
//        {
//            Intent i = new Intent(CreateNewNotification.this, TaskList.class);
//            CreateNewNotification.this.startActivity(i);
//        }catch(Exception e){
//
//        }
    }

    private void sendNewNotificationToServer()
    {
        new Thread(new Runnable() {
            @Override
            public void run() {
                HttpRequester http = new HttpRequester();

                String[] hej = new String[3];
                TextView txtTitle = (TextView) findViewById(R.id.txtTitle);
                hej[0] = txtTitle.getText().toString();
                hej[1] = "default description";
                hej[2] = "2014-08-25";
                http.doInBackground(hej);
            }
        }).start();
    }
}
