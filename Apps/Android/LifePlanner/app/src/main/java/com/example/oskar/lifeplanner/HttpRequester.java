package com.example.oskar.lifeplanner;

import android.os.AsyncTask;
import android.util.Log;

import org.apache.http.HttpResponse;
import org.apache.http.ParseException;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.message.BasicHeader;
import org.apache.http.protocol.HTTP;
import org.apache.http.util.EntityUtils;
import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONTokener;

import java.io.UnsupportedEncodingException;

/**
 * Created by Oskar on 2014-07-30.
 */
public class HttpRequester extends AsyncTask<String, Void, String> {
    @Override
    protected String doInBackground(String... strings) {

        String title = strings[0];
        String description = strings[1];
        String date = strings[2];

        HttpClient httpclient = new DefaultHttpClient();
        HttpPost post = new HttpPost("http://192.168.1.64:6060/api");

        JSONObject jsonObject = getJsonObject(title, description);

        StringEntity se = getHttpData(jsonObject);
        if (se == null) {
            return "error creating HttpData";
        }
        post.setEntity(se);

        return sendRequest(httpclient, post);
    }

    private String sendRequest(HttpClient httpclient, HttpPost post) {
        try {
            HttpResponse httpresponse = httpclient.execute(post);
            String responseText = null;
            responseText = EntityUtils.toString(httpresponse.getEntity());
            Log.i("result", responseText);

            return responseText;
        } catch (ParseException e) {
            e.printStackTrace();
            Log.i("Parse Exception", e + "");
        } catch (Exception e) {
            e.printStackTrace();
            Log.i("Exception", e + "");
        }

        return "error while making request";
    }

    private JSONObject getJsonObject(String title, String description) {
        String json = getJsonString(title, description);
        JSONObject jsonObject = new JSONObject();
        try {
            jsonObject = (JSONObject) new JSONTokener(json).nextValue();
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return jsonObject;
    }

    private String getJsonString(String title, String description) {
        return "{"
                    +       "\"data\": {"
                    +           "\"title\": \"" + title + "\""
                    +           "\"description\": \"" + description + "\""
                    +       "}"
                    +     "}";
    }

    private StringEntity getHttpData(JSONObject jsonObject)
    {
        StringEntity se;
        try {
            se = new StringEntity(jsonObject.toString());
        } catch (UnsupportedEncodingException e) {
            e.printStackTrace();
            return null;
        }
        se.setContentType("application/json;charset=UTF-8");
        se.setContentEncoding(new BasicHeader(HTTP.CONTENT_TYPE,"application/json;charset=UTF-8"));

        return se;
    }
}
