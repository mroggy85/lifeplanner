/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef:true */
/*jslint vars:true */
/*jslint plusplus: true */

(function () {
    "use strict";
    
    var PORT = process.env.PORT || 6061;
    
    console.log('PUBSUB: Init');

    var express = require('express');
    var path = require('path');
    var logger = require('morgan');
    var bodyParser = require('body-parser');
    var routes = require('../../Presentation/pubsub/routes.js');

    
    var app = express();

    app.use(logger('dev'));
    app.use(bodyParser.json());

    app.use('/pubsub', routes);

    app.set('port', PORT);
    
    app.listen(app.get('port'), function () {
        console.log('Express server listening on port ' + PORT);
    });
}());