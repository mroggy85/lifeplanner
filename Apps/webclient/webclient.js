/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef:true */
/*jslint vars:true */
/*jslint plusplus: true */

(function () {
    "use strict";
    
    var PORT = process.env.PORT || 6062;
    
    console.log('WEB: Init');
    
    var app = require('../../Presentation/webclient/app.js');

    app.set('port', PORT);
    
    app.listen(app.get('port'), function () {
        console.log('WebClient server listening on port ' + PORT);
    });
}());