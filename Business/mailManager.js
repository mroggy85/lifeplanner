/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef:true */
/*jslint vars:true */
/*jslint plusplus: true */
/*jslint white: true */

var nodeMailer = require('nodemailer');

/* Comment of class
 *
 */
var mailManager = function () {
    "use strict";
    
    // PRIVATE MEMBERS
    var _transporter;
    
    // METHODS
    // -- public
    var SendNotificationMail,
    // -- private
        createTransporter,
        sendMail,
        getNotificationMail;
    
    // INIT
    var init = function () {
        createTransporter();
    };
    
    // PUBLIC METHODS
    
    SendNotificationMail = function (items) {
        var mailOptions = getNotificationMail(items);
        sendMail(mailOptions);
    };
    
    
    // PRIVATE METHODS
    
    createTransporter = function () {
        _transporter = nodeMailer.createTransport({
            service: 'Gmail',
            auth: {
                user: 'lindgren.oskar@gmail.com',
                pass: 'Brumme1337'
            }
        });
    };
    
    sendMail = function (mailOptions) {
        console.log('sending mail...');
        _transporter.sendMail(mailOptions, function (err, info) {
            if (err) {
                console.log(err);
            }
            else {
                console.log('Message sent: ' + info.response);
            }
        });
    };
    
    getNotificationMail = function (items) {
        var mailOptions = {
            from: 'Oskar Lindgren <lindgren.oskar@gmail.com>',
            to: 'lindgren.oskar@gmail.com',
            subject: 'New Notification from RMDR',
            text: 'You have a new notification. Amount: ' + items.length,
            html: '<b>Hello</b>'
        };
        
        return mailOptions;
    };
    
    // FASCADE
    var fascade = {
        
        "sendNotificationMail": SendNotificationMail
    };
    
    init();
    
    return fascade;
};

module.exports = mailManager;