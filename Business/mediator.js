/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef:true */
/*jslint vars:true */
/*jslint plusplus: true */
/*jslint es5: true */

var request = require("request");

var mediator = function (debugging) {
    "use strict";
    
    // PRIVATE MEMBERS
    var channels = {}; // Storage for our topics/events
    
    // METHODS
    var Subscribe, Publish,
        handleResponse, debugInfo;
    
    // PUBLIC METHODS

    // Subscribe to an event, supply a callback to be executed 
    // when that event is broadcast
    Subscribe = function (channel, data) {
        
        if (!channels[channel]) {
            channels[channel] = [];
        }
        channels[channel].push({ context: this, url: data.url, method: data.method });
        
        return this;
    };

    // Publish/broadcast an event to the rest of the application
    Publish = function (channel, data) {
        debugInfo(channel);
        
        //var args = Array.prototype.slice.call(arguments, 1);
        var i, l = (channels[channel] ? channels[channel].length : 0);
        for (i = 0; i < l; i++) {
            var subscription = channels[channel][i];
            //subscription.callback.apply(subscription.context, args);
            
            console.log('subscription.url: ' + subscription.url);
            
            request({
                uri: subscription.url,
                method: "GET",
                form: data,
                timeout: 10000,
                followRedirect: true,
                maxRedirects: 10
            }, handleResponse);
        }

        if (debugging && console.group) {
            console.groupEnd();
        }
        return this;
    };
    
    // PRIVATE METHODS
    
    handleResponse = function (error, response, body) {
        console.log(body);
    };
    
    debugInfo = function (channel) {
        if (debugging && console.group) {
            console.group();
        }
        
        if (debugging) {
            var length = (channels[channel] ? channels[channel].length : 0);
            console.log('[publish:' + channel + '] - ' +
                length + ' subscriber(s)');
        }
    };

    var fascade = {
        publish: Publish,
        subscribe: Subscribe,
        installTo: function (obj) {
            obj.subscribe = Subscribe;
            obj.publish = Publish;
        }
        
        // --- MEDIATOR EVENTS ---
        
        
    };
    
    if (debugging) {
        fascade.channels = channels;
    }

    return fascade;
};

module.exports.getMediator = mediator;