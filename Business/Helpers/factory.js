/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef:true */
/*jslint vars:true */
/*jslint plusplus: true */
/*jslint es5: true */

var reminder = require('../../Data/Objects/reminder.js');
var toBeNotifed = require('../../Data/Objects/toBeNotified.js');

var factory = function () {
    "use strict";
    
    var CreateReminder = function () {
        return reminder.DefaultObject;
    };
    
    var CreateToBeNotified = function () {
        return toBeNotifed.DefaultObject;
    };
    
    var fascade = {
        "getDefaultReminder": CreateReminder,
        "getDefaultToBeNotified": CreateToBeNotified
    };
    
    return fascade;
};

module.exports.getFactory = factory;