/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef:true */
/*jslint vars:true */
/*jslint plusplus: true */
/*jslint white: true */
/*jslint es5: true */



/* API Requester
 * ----------------------------------
 * An interface for communication with the API
 */
var apiRequester = function () {
    "use strict";
    
    // IMPORTS
    var request = require("request");
    var unirest = require('unirest');
    var headerTypeEnum = require('../Data/Enums/headerTypeEnum.js');
    var headerValueEnum = require('../Data/Enums/headerValueEnum.js');
    
    // PRIVATE MEMBERS
    var _req = request;
    var _req2 = unirest;
    
    // CONSTANTS
    var API_PORT = "6060",
        API_URI = "http://localhost:" + API_PORT + "/api";
    
    
    // METHODS
    // -- public
    var GetReminderByID,
        GetAllReminders,
        CreateReminder,
        ChangeReminder,
        DeleteReminder;
    // -- private
    var parseBodyToJSON;
    
    // INIT
    var init = function () {
        
    };
    
    // PUBLIC METHODS
    
    GetReminderByID = function (id, callback) {
        _req({
            uri: API_URI + "/?id=" + id,
            method: "GET",
            headers: {
                "Accepts": headerValueEnum.JSON
            }
        }, function (err, res, body) {
            if (err) {
                throw new Error(err);
            }
            var item = parseBodyToJSON(body);
            callback(item);
        });
    };
    
    GetAllReminders = function (callback) {
        _req({
            uri: API_URI + "/all",
            method: "GET",
            headers: {
                "Accepts": headerValueEnum.JSON
            }
        }, function (err, res, body) {
            if (err) {
                throw new Error(err);
            }
            var items = parseBodyToJSON(body);
            callback(items);
        });
    };
    
    CreateReminder = function (data, callback) {
        data = { data: data};
        
        _req({
            uri: API_URI + "/",
            method: "POST",
            body: JSON.stringify(data),
            headers: {
                "Accepts": headerValueEnum.JSON,
                "Content-Type": "application/json"
            }
        }, function (err, res, body) {
            if (err) {
                throw new Error(err);
            }
            var items = parseBodyToJSON(body);
            callback(items);
        });
    };
    
    ChangeReminder = function (data, callback) {
        data = { data: data};
        
        _req({
            uri: API_URI + "/",
            method: "PUT",
            body: JSON.stringify(data),
            headers: {
                "Accepts": headerValueEnum.JSON,
                "Content-Type": "application/json"
            }
        }, function (err, res, body) {
            if (err) {
                throw new Error(err);
            }
            var items = parseBodyToJSON(body);
            callback(items);
        });
    };
    
    DeleteReminder = function (id, callback) {
        
        _req2.delete(API_URI)
        .headers({ 
            'Accept': 'application/json',
            'content-type': 'application/json'
        })
        .send({ "id": id })
        .end(function (response) {
          callback(response);
        });
    };
    
    
    // PRIVATE METHODS
    
    parseBodyToJSON = function (body) {
        try {
            return JSON.parse(body);
        }
        catch (ex) {
            console.error(ex);
            return [];
        }
    };
    
    // FASCADE
    var fascade = {
        "getReminderByID" : GetReminderByID,
        "getAllReminders" : GetAllReminders,
        "createReminder" : CreateReminder,
        "changeReminder" : ChangeReminder,
        "deleteReminder" : DeleteReminder
    };
    
    init();
    
    return fascade;
};

module.exports = apiRequester;