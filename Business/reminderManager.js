/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef:true */
/*jslint vars:true */
/*jslint plusplus: true */
/*jslint es5: true */

var events = require('events');
var factory = require('./Helpers/factory.js');
var reminderPersister = require('../Data/Managers/reminderPersister.js');
var eventsEnum = require('../Data/Enums/eventsEnum.js').eventsEnum;
var uuid = require('node-uuid');

var reminderManager = function () {
    "use strict";
    
    // PRIVATE OBJECTS
    var _factory = factory.getFactory(),
        _reminderPersister = reminderPersister.getReminderPersister();
    
    // METHODS
    var CreateNewReminder, ChangeReminder, GetReminderByID, GetAllReminders, RemoveReminderByID,
        isUserDataValid, setDataFromUserReminder, setDataFromUserToBeNotifed;
    
    // INIT
    var init = function () {
        console.log('RMDR_MGR: Init');
    };
    
    // PUBLIC METHODS
    
    CreateNewReminder = function (dataFromUser, callback) {
        console.log('RMDR_MGR: Creating reminder...');
        isUserDataValid(dataFromUser);
        
        var reminder = _factory.getDefaultReminder();
        setDataFromUserReminder(reminder, dataFromUser);
        
        var guid_reminder = uuid.v4();
        reminder._id = guid_reminder;
        
        
        //_reminderPersister.create(reminder, callback);
        _reminderPersister.createNewReminder(reminder, function (item) {
            var toBeNotified = _factory.getDefaultToBeNotified();
            setDataFromUserToBeNotifed(toBeNotified, dataFromUser, item._id);
            
            var guid_notification = uuid.v4();
            toBeNotified._id = guid_notification;
            
            _reminderPersister.createNewNotification(toBeNotified, callback);
        });
    };
    
    ChangeReminder = function (dataFromUser, callback) {
        console.log('RMDR_MGR: Changing reminder...');
        _reminderPersister.update(dataFromUser, callback);
    };
    
    GetReminderByID = function (id, callback) {
        console.log('RMDR_MGR: Getting reminder...');
        _reminderPersister.read(id, callback);
    };
    
    GetAllReminders = function (callback) {
        console.log('RMDR_MGR: Getting all reminders...');
        _reminderPersister.getAll(callback);
    };
    
    RemoveReminderByID = function (id, callback) {
        console.log('RMDR_MGR: Deleting reminder...');
        _reminderPersister.remove(id, callback);
    };
    
    
    // PRIVATE METHODS
    
    isUserDataValid = function (data) {
        console.log('RMDR_MGR: validating data...');
        
        if (!data) {
            throw new Error("No data from user");
        }
        
        if (!data.title && !data.description) {
            throw new Error("Title and Desciption cannot both be empty");
        }
        
        return true;
    };
    
    setDataFromUserReminder = function (defaultReminder, dataFromUser) {
        console.log('RMDR_MGR: setting data...');
        
        if (dataFromUser.title) {
            defaultReminder.title = dataFromUser.title;
        }
        if (dataFromUser.description) {
            defaultReminder.description = dataFromUser.description;
        }
    };
    
    setDataFromUserToBeNotifed = function (defaultToBeNotified, dataFromUser, reminderId) {
        console.log('RMDR_MGR: setting data...');
        
        defaultToBeNotified.reminderID = reminderId;
        
        if (dataFromUser.datetoshow) {
            var dateArr = dataFromUser.datetoshow.split('-');
            var date = new Date(dateArr[0], dateArr[1] - 1, dateArr[2]);
            defaultToBeNotified.dateToShow = date;
        }
        if (dataFromUser.timetoshow) {
            defaultToBeNotified.timeToShow = dataFromUser.timetoshow;
        }
    };
    
    // --------------
    
    var fascade = {
        "createReminder": CreateNewReminder,
        "changeReminder": ChangeReminder,
        "getReminderByID": GetReminderByID,
        "getAllReminders": GetAllReminders,
        "removeReminderByID": RemoveReminderByID
    };
    
    init();
    
    return fascade;
};

module.exports.getReminderManager = reminderManager;