/*jslint node: true */
/*jslint nomen: true*/
/*jslint undef:true */
/*jslint vars:true */
/*jslint plusplus: true */
/*jslint es5: true */
/*jslint white: true */

var events = require('events');
var factory = require('./Helpers/factory.js');
var reminderPersister = require('../Data/Managers/reminderPersister.js');
var eventsEnum = require('../Data/Enums/eventsEnum.js').eventsEnum;
var uuid = require('node-uuid');
var mailManager = require('./mailManager');

var reminderMonitor = function () {
    "use strict";
    
    // CONSTANTS
    
    // PRIVATE OBJECTS
    var _eventEmitter = new events.EventEmitter(),
        _factory = factory.getFactory(),
        _reminderPersister = reminderPersister.getReminderPersister(null, 'toBeNotified'),
        _mailManager = mailManager();
    
    // METHODS
        // -- public
    var CheckIfAnyReminderIsReady,
        // -- private
        handleRemindersToBeNotified,
        sendMail,
        removeItemsFromQue,
        handleRemoveItemsFromQue;
    
    // INIT
    var init = function () {
        console.log('RMDR_MON: Init');
    };
    
    // PUBLIC METHODS
    
    CheckIfAnyReminderIsReady = function () {
        console.log('RMDR_MON: Check!');
        
        _reminderPersister.GetRemindersBeforeDate(new Date(), handleRemindersToBeNotified);
        
    };
    
    
    // PRIVATE METHODS
    
    handleRemindersToBeNotified = function (items) {
        console.log('RMDR_MON: Check completed!');
        console.log(items.length);
        if (items.length === 0) {
            return;
        }
        
        removeItemsFromQue(items);
        
        console.dir(items);
        sendMail(items);
    };
    
    removeItemsFromQue = function (items) {
        var i = 0;
        var l = items.length;
        for (i; i < l; i++) {
            var item = items[i];
            
            _reminderPersister.remove(item._id, handleRemoveItemsFromQue);
        }   
    };
    
    handleRemoveItemsFromQue = function () {
        
    };
    
    sendMail = function (items) {
        console.log('preparing to send mail...');
        
        _mailManager.sendNotificationMail(items);
    };
    

    // --------------
    
    var fascade = {
        "check": CheckIfAnyReminderIsReady
    };
    
    init();
    
    return fascade;
};

module.exports.getReminderMonitor = reminderMonitor;